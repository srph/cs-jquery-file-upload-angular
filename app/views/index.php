<!doctype html>
<html lang="en" ng-app="upp">
<head>
	<meta charset="UTF-8">
	<title>Laravel PHP Framework</title>
	<link type="text/css" rel="stylesheet" href="vendor/bootstrap-dist/css/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" href="vendor/jquery-file-upload/css/jquery.fileupload.css">
	<style>
		.container {
			width: 1024;
		}
	</style>
	
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<h1> Upload </h1>
						<hr>

						<!-- <form id="fileupload"
							action="/"
							method="POST"
							enctype="multipart/form-data"
							data-ng-controller="UpCtrl"
							data-file-upload="options"
							data-ng-class="{'fileupload-processing': processing() || loadingFiles}">

							<span class="fileupload-buttonbar">
								<button type="button" class="btn btn-success fileinput-button" ng-class="{disabled: disabled}">
									<i class="glyphicon glyphicon-plus"></i>
									<span> Add Files </span>
									<input type="file" name="files[]" ng-disabled="disabled">
								</button>
							</span>

							<button type="button" class="btn btn-primary start" data-ng-click="submit()">
								<i class="glyphicon glyphicon-upload"></i>
									<span>Start upload</span>
							</button>

							<button type="button" class="btn btn-warning cancel" data-ng-click="cancel()">
								<i class="glyphicon glyphicon-ban-circle"></i>
								<span>Cancel upload</span>
							</button>

							<span class="fileupload-process"></span>

							<table class="table table-striped files ng-cloak">
								<tr data-ng-repeat="file in queue" data-ng-class="{'processing': file.$processing()}">
									<td data-ng-switch data-on="!!file.thumbnailUrl">
										<div class="preview" data-ng-switch-when="true">
											<a data-ng-href="{{file.url}}" title="{{file.name}}" download="{{file.name}}" data-gallery><img data-ng-src="{{file.thumbnailUrl}}" alt=""></a>
										</div>
										<div class="preview" data-ng-switch-default data-file-upload-preview="file"></div>
									</td>
									<td>
										<p class="name" data-ng-switch data-on="!!file.url">
											<span data-ng-switch-when="true" data-ng-switch data-on="!!file.thumbnailUrl">
												<a data-ng-switch-when="true" data-ng-href="{{file.url}}" title="{{file.name}}" download="{{file.name}}" data-gallery>{{file.name}}</a>
												<a data-ng-switch-default data-ng-href="{{file.url}}" title="{{file.name}}" download="{{file.name}}">{{file.name}}</a>
											</span>
											<span data-ng-switch-default>{{file.name}}</span>
										</p>
										<strong data-ng-show="file.error" class="error text-danger">{{file.error}}</strong>
									</td>
									<td>
										<p class="size">{{file.size | formatFileSize}}</p>
										<div class="progress progress-striped active fade" data-ng-class="{pending: 'in'}[file.$state()]" data-file-upload-progress="file.$progress()"><div class="progress-bar progress-bar-success" data-ng-style="{width: num + '%'}"></div></div>
									</td>
									<td>
										<button type="button" class="btn btn-primary start" data-ng-click="file.$submit()" data-ng-hide="!file.$submit || options.autoUpload" data-ng-disabled="file.$state() == 'pending' || file.$state() == 'rejected'">
											<i class="glyphicon glyphicon-upload"></i>
											<span>Start</span>
										</button>
										<button type="button" class="btn btn-warning cancel" data-ng-click="file.$cancel()" data-ng-hide="!file.$cancel">
											<i class="glyphicon glyphicon-ban-circle"></i>
											<span>Cancel</span>
										</button>
										<button data-ng-controller="FileDestroyController" type="button" class="btn btn-danger destroy" data-ng-click="file.$destroy()" data-ng-hide="!file.$destroy">
											<i class="glyphicon glyphicon-trash"></i>
											<span>Delete</span>
										</button>
									</td>
								</tr>
							</table>
						</form> -->

						<upload></upload>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="vendor/jquery/dist/jquery.min.js"></script>
	<script src="vendor/bootstrap-dist/js/bootstrap.min.js"></script>
	<script src="vendor/angular/angular.min.js"></script>
	<script src="vendor/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
	<script src="vendor/jquery-file-upload/js/jquery.iframe-transport.js"></script>
	<script src="vendor/jquery-file-upload/js/jquery.fileupload.js"></script>
	<script src="vendor/jquery-file-upload/js/jquery.fileupload-angular.js"></script>
	<script type="text/javascript" src="app/app.js"></script>
</body>
</html>