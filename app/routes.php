<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('index');
});

Route::post('file-upload', function()
{
	$file = Input::file('file');
	$name = $file->getClientOriginalName();
	$path = 'files';

	$file->move($path, $name);

	return Response::json(array(
		'input' =>	Input::file('file'),
		'file'	=>	Input::hasFile('file')
	));
});