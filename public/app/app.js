'use strict';

var app = angular.module('upp', ['blueimp.fileupload']);

app.directive('upload', function() {
	return {
		templateUrl: 'app/upload.html',
		restrict: 'E',
		controller: 'UpCtrl',
		link: function postLink(scope, el, att) {
			var input = $(el).find('input[type=file]');

			input.fileupload({
				dataType: 'json',
				paramName: 'file',
				add: function(e, data) {
					scope.$apply(function() {
						scope.files.push(data);
					});
				},
				progress: function(e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					scope.$apply(function() {
						scope.percentage = progress;
					});
				},
				done: function(e, data) {
					console.log('Done!');
				},
				success: function(data) {
					console.log(data);
				}
			});
		}
	};
});

app.controller('UpCtrl', function($scope, $http) {
	$scope.files = [];
	$scope.percentage = 0;

	$scope.upload = function() {
		if($scope.files.length) {
			console.log('Uploading..');
			console.log($scope.files[0]);
			$scope.files[0].submit();
			$scope.files = [];
		} else {
			console.log('No files..');
		}
	}
});